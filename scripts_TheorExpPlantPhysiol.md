
### The annotation file on NCBI, from Vitis vinifera (PN40024) was explored to obtain minimum and maximum lines through the calculation of marker mapping position, minus or plus 700 Kb, respectively (example using marker CTG1012992 in chromosome 01): ###

### minimum line: 10812720 − 700000 = 10112720 ###

` grep "chr01" vitis_ncbi_annotations_on_12X_V2.gff3 | grep ID=gene | grep -n "^\S\+\s\+\S\+\s\+\S\+\s\+1011[0-9][0-9][0-9][0-9]" | head `

(862)

### maximum line: 10812994 + 700000 = 11512994 ###

`grep "chr01" vitis_ncbi_annotations_on_12X_V2.gff3 | grep ID=gene | grep -n "^\S\+\s\+\S\+\s\+\S\+\s\+115[0-3][0-9][0-9][0-9][0-9]" | head `

(953)

### Extract the gene labels contained in the proximity of the marker, based on the previous determined minimum and maximum lines: ###

` grep "chr01" vitis_ncbi_annotations_on_12X_V2.gff3 | grep ID=gene | sed -n '862,953p' | sed 's/;/\t/g' | cut -f1,4-5,10 | sed 's/Dbxref=GeneID://g' | sed 's/\t/,/g' > CTG1012992_gene_label.csv `

### The same steps were repeated for each marker, and then used to construct the input file for evaluating functional enrichment in [shinyGO](http://bioinformatics.sdstate.edu/go/) ###

### Select all markers for functional enrichment in shinyGO v0.96 ###

` cat *_gene_label.csv | cut -f4 -d',' | sort -u > all_mark_gene_labels_ID `
