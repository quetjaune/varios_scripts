grep -f ID_genes_Ine_4_circos vitis_ncbi_annotations_on_12X_V2.gff3 | grep ID=gene | sed 's/;/\t/g' | cut -f1,4-5,10 | sed 's/Dbxref=GeneID://g' | sed 's/\t/,/g' > gene_label.csv

#Select each marker by + or - 700Kb from primers hibrydization sites in cat coord_marcadores_chr_vit_vinif.txt (example with CTG1012992_SLA_Canopia_DWR_Biomasa_total in chr01)

#minimum line
10812720−700000=10112720
grep "chr01" vitis_ncbi_annotations_on_12X_V2.gff3 | grep ID=gene | grep -n "^\S\+\s\+\S\+\s\+\S\+\s\+1011[0-9][0-9][0-9][0-9]" | head
#maximum line
10812994+700000=11512994
grep "chr01" vitis_ncbi_annotations_on_12X_V2.gff3 | grep ID=gene | grep -n "^\S\+\s\+\S\+\s\+\S\+\s\+115[0-3][0-9][0-9][0-9][0-9]" | head 

#get gene labels in the range of the marker + or - 700 Kb
grep "chr01" vitis_ncbi_annotations_on_12X_V2.gff3 | grep ID=gene | sed -n '862,953p' | sed 's/;/\t/g' | cut -f1,4-5,10 | sed 's/Dbxref=GeneID://g' | sed 's/\t/,/g' > CTG1012992_SLA_Canopia_DWR_Biomasa_total_gene_label.csv

#select all markers for funtional enrichment in shinyGO v0.96
cat *_gene_label.csv | cut -f4 -d',' | sort -u > all_mark_gene_labels_ID

#select only significant markers for funtional enrichment in shinyGO v0.60
cat CTG1011026_PESO_PODA_gene_label.csv AF143277_DWR_DWL_gene_label.csv CTG1030395_LA_Canopia_DWL_Biomasa_total_gene_label.csv CTG1011026_PESO_PODA_gene_label.csv UDV095_PESO_PODA_gene_label.csv VMC9a2.1_Chr19_LA_Tbiomass.txt | cut -f4 -d',' | sort -u > all_significant_mark_gene_labels_ID

#agregando los nuevos marcadores del Chr10 (correo de octubre)
cat all_significant_mark_gene_labels_ID CTG1029421_PESO_PODA_gene_label.csv VMC3d7_PESO_PODA_gene_label.csv VrZAG64_67_PESO_PODA_gene_labels.csv | cut -f4 -d',' | sort -u > signif_mark_plus_Chr10_oct2019_ID



RETOMANDO 21/10/2019; agregar lista de genes de los nuveos marcadores de peso de poda (CTG1029421; VMC3d7; VrZAG64; VrZAG67) todos en Chr10

CTG1029421
(GGTTGACTGTCATTTCATTAGAGAGAAGATCGCATCAGGATGTGTTGCTACAAGTTTTGTCAATTCAAATGATCAACTAGCAGACATCTTCACTAAATCTCTCAGAGGTCCTAGGATTAAATATATTTGTAACAAGCTTGGTGCATATGACGTATATGCTCCAGCTTGAGGGGGAGTGTTGAATATAATGTATGTATAGTATACTATCTTTCCTTGTTAATATAGGTCACA
TGTATGGTAGTTAGGACTCCTAGCCTTGTATATATATATCTCTCAATTGTAAGTAGAGATTACAATGAATGTGAATAAGATTTTTCTCCTTTCTCTCTCTCTCTCTCAACAATAAACACATGCTAAAATCAATGAAATATGCAACATAATTAACTAACATACCTGATGTCATATATGCAAATTTTAGTAAAAACAATACATGTTAGTTAATAAAAAAAGAGTAACATTTTAT
ACATTCATGCTCATATGTGTATATATGTGTATATATATATATATATATGTATCCATGCAATAAAGTGAAACCTAGAACCTCCCCTTAGCCAAGTCTAAGGGCTTGGGAGAAGAAAGACTCATTTTGANAGAAGCAAATTACAAACTTTTTTATAGCTAAACATTANAAG)

#minimum line
3243933-700000=2543933 (141)
#maximum line
3244277+700000=3944277 (148)

grep "chr10" vitis_ncbi_annotations_on_12X_V2.gff3 | grep ID=gene | sed -n '141,148p' | sed 's/;/\t/g' | cut -f1,4-5,10 | sed 's/Dbxref=GeneID://g' | sed 's/\t/,/g' > CTG1029421_PESO_PODA_gene_label.csv

VMC3d7 (FW:gcAccATTcTgccATTATgATT; RV: TATTTTcAgTgAcccAcTcTTcAc)
#minimum line
1080383-700000=380383 (42)
#maximum line
1080531+700000=1780531 (140)

grep "chr10" vitis_ncbi_annotations_on_12X_V2.gff3 | grep ID=gene | sed -n '42,140p' | sed 's/;/\t/g' | cut -f1,4-5,10 | sed 's/Dbxref=GeneID://g' | sed 's/\t/,/g' > VMC3d7_PESO_PODA_gene_label.csv


VrZAG64 (FW:tatgaaagaaacccaacgcggcacg; RV: tgcaatgtggtcagcctttgatggg)
4898590-700000=4198590 (149)
4898729+700000=5598729 (221)

grep "chr10" vitis_ncbi_annotations_on_12X_V2.gff3 | grep ID=gene | sed -n '149,221p' | sed 's/;/\t/g' | cut -f1,4-5,10 | sed 's/Dbxref=GeneID://g' | sed 's/\t/,/g' > VrZAG64_PESO_PODA_gene_labels.csv


VrZAG67 (FW: acctggcccgactcctcttgtatgc; RV: tcctgccggcgataaccaagctatg) SOLAPA AL ANTERIOR


5006368-700000=4306368 (149)
5006521+700000=5706521 (234)

grep "chr10" vitis_ncbi_annotations_on_12X_V2.gff3 | grep ID=gene | sed -n '149,234p' | sed 's/;/\t/g' | cut -f1,4-5,10 | sed 's/Dbxref=GeneID://g' | sed 's/\t/,/g' > VrZAG64_67_PESO_PODA_gene_labels.csv



