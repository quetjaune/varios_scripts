#hacer limpieza de adaptadores

for file in *.fastq; do fastx_clipper -a "TCGTCGGCAGCGTCAGATGTGTATAAGAGACAG"-i $file -o $file.clean.fastq; done

# limpieza de adaptadores con cutadapt

for file in *fastq; do perl /LUSTRE/bioinformatica_data/genomica_funcional/bin/cutadapt-1.4.2/bin/cutadapt -b TCGTCGGCAGCGTCAGATGTGTATAAGAGACAG $file > $file.clean.fq ; done

#recorte de secuencias con base en su calidad

for file in *R1*001.fastq; do /home/paoline/fastq_quality_trimmer -v -t 33 -l 100 -i $file -o $file.trim.fq -Q33; done

#hacer reverso complementario

for file in *clean.fastq; do fastx_reverse_complement -i $file -o $file.rc.fastq; done

#cabiar nombre del archivo metadato de input en mothur

sed -i 's/fastq.rc.clean.fastq/fastq.rc.fastq.clean.fastq/g' r16042016cl.files 


###USANDO CUTADAPT
for file in *.fastq; do cutadapt -g TTGTACACACCGCCC $file > $file.noprim.fastq; done

###para reverso complementario

for file in *rc*.fastq; do ../../../bin/cutadapt-1.4.2/bin/cutadapt -a GGGCGGTGTGTACAA  $file > $file.noprim.fastq; done


#####cortar, ordenar y contar únicas#####

cut -f3 *align.report | sort | uniq -c | sort -n | more

sort -u -k2,2  w2_OK.tax > w2_OK_nodup.tax

####grep IGNORE CASES####

 grep -c "gatgaatggcttagtgagaattttggagagatttatttagaggcgactctttttaaatttcaaagttattcaaacttggtgatttagaggaagtaaaagtcgtaacaag" --ignore-case r16042016NP.trim.contigs.subsample.*fasta


####EXTRAER secuencias de archivo fasta#####

xargs faidx -d ' ' w2_OK.fasta <ID_w2_nodup > w2_OK_nodup.fasta


######SPLIT FILES in length l####
split -l 500 myfile segment

####print Headers in fasta####

grep ">" w2_OK_nodup_good.clustalign.fasta | sed 's/>//g' > ID_w2_nodup_good


##########INSTALAR PROGRAMAS EN OMICA############

wget 'http://www.clustal.org/omega/clustalo-1.2.0-Ubuntu-x86_64'

mv clustalo-1.2.0-Ubuntu-x86_64 clustalo

chmod +x clustalo 

Agregar path al archivo .profile





####para ver los procesadores en omica#####

sacct -j 4385

######ASK for a package#####

dpkg -L mothur-mpi 


#####run clustalo with multiprocesadores en ixachi#####
nohup mpirun -n 16 clustalo --force --threads=16 -i w2_OK_97.fasta -o w2_OK_97_clustaligned.fasta &

######limpiar archivos fasta de la presencia de caracteres raros####

sed -e '/^[^>]/s/[^ATGCatgc]/N/g' file.fa


###edicion de archivo de rarefaccion para phylotype #####

cut -f1,2,5,8,11,14,17,20,23,26,29,32,35,38,41,44,47,50,53,56,59,62,65,68,71,74,77,80,83,86,89,92,95,98,101,104,107,110,113,116,119,122,125,128,131,134,137,140,143,146,149,152,155,158,161,164,167,170,173,176,179,182,185,188,191,194,197,200,203,206,209,212,215,218,221,224,227,230,233,236,239 *wang*rarefaction | sed 's/1-//g' > phylo_raref_edit.txt

###edicion de archivo de rarefaccion para otus #####
cut -f1,2,5,8,11,14,17,20,23,26,29,32,35,38,41,44,47,50,53,56,59,62,65,68,71,74,77,80,83,86,89,92,95,98,101,104,107,110,113,116,119,122,125,128,131,134,137,140,143,146,149,152,155,158,161,164,167,170,173,176,179,182,185,188,191,194,197,200,203,206,209,212,215,218,221,224,227,230,233,236,239 *unique_list.groups.rarefaction | sed 's/0.03-//g' > otu_raref_edit.txt

#####separar columnas de acuerdo al delimitador##############

cut -f9 -d ';' *tx.1.cons.taxonomy | sort | uniq | grep '(100)'| more


####cortar lineas específicas de archivo y pegar como columnas en nuevo archivo######NO FUNCIONO##

sed -n '1,1p' *unique_list.list | fmt -1 > A

sed -n '5,5p' *unique_list.list | fmt -1 > B

paste A B > 97_OTU_rep_sec 

######diferencias en IDs de dos archivos fasta######NO EXTRAE LA SEC NUCLEOTIDICA

diff <(cat  r01072016pe.trim.contigs.good.unique.fasta | grep ">" | sort)  <(cat r01072016pe.trim.contigs.good.unique.good.filter.fasta | grep ">" | sort)  | grep "^<" | awk -F\> '{print $2}' > unaligned_unique_seqs.fasta


####extraer secuencias especificas de multifasta con lista de IDs######

pyfasta extract --header --fasta r01072016pe.trim.contigs.good.unique.fasta --file ID_unaligned_seqs > unaligned_unique_seqs.fasta

#####convert files "" to Tab####

sed 's/"*"/\t/g' OTUs97_InKrona.txt > InKrona.txt


####USO DE KRONA#######

ktImportText OTUs97_InKrona.txt -o OTUs97_InKrona.html


#####filtrar tabla con valores mayores a un umbral y preparar archivo de taxonomia de mothur para el input en Krona#######
awk '(NR>1) && ($2 > 4 ) ' *unique_list.unique.cons.taxonomy | cut -f2,3 > filt_h5_unique_ab_tax

sed 's/([0-9][0-9]*);/\t/g' filt_h5_unique_ab_tax > InKrona_unique.txt


ktImportText InKrona_unique.txt -o OTUs_unique_Krona.html


####extracción de archivos con reciente modificación ########

find /LUSTRE/bioinformatica_data/genomica_funcional/paoline/XIXIMI/run_01_07_2016/r01072016pe.* -mtime -1 -exec ls -ltc {} \; | grep "r01072016pe" | grep "Sep" > 4scp_lmf_ID

####copia de lista de archivos desde OMICA ####

for file in `cat 4scp_lmf`; do scp paoline@omica:"$file" ./ ; done 

##desde archivos comprimidos a partir de una lista####
tar -cvf prec1.tar -T 4scp_lmf

####adaptar archivos para correr swarm a partir de mothur (juntando *unique.fasta y unique.count_table o de metabarcodes)#####
python /home/marcos/Documents/scripts/fasta_head_replace.py 

######corrida SWARM###

/home/marcos/Documents/SWARM/swarm/bin/swarm swarm_in.fasta > swarm_clust_out 

######crear INPUT SWARM desde metabarcodes####

cut -f1-2  *uchime.pick.count_table > total_abund_seq_post_prec

sed 's/_/-/g' total_abund_seq_post_prec | sed 's/\t/_/g' > swarm_head_in_postprec

python /home/marcos/Documents/scripts/fasta_head_replace.py 

/home/marcos/Documents/SWARM/swarm/bin/swarm swarm_postprec_in.fasta > swarm_out_postprec

######Modificar headers de archivos fastq para input en Trinity#########
for lib in *_R2.trim.unpaired.fastq

do

cat $lib | sed 's/-2:/:/;n;n;n' | sed '1~4 s/$/\/2/g' > ${lib/.trim/.scrub.fixed}

done

####Cut by tab delimiter columns#######
Press Ctrl-v + Tab

cut -f2 -d'   ' infile
or write it like this:

cut -f2 -d$'\t' infile


#####formatear archivos output qiime para grafica de Krona#######

join -o 1.2 2.2 <(sort otu_table_mc2.txt) <(sort .uclust_assigned_taxonomy/rep_set_tax_assignments.txt) > open_otus_count_tax

sed -i 's/ /\t/g' open_otus_count_tax 

sed 's/;/\t/g' open_otus_count_tax > InKrona.txt

ktImportText InKrona.txt -o OTUs_open_qiime_Krona.html

######copiar sólo archivos de reciente modificación (1 día) #########################

find . -mtime -1 -exec cp {} /agc_clustersplit/ \;


####list only directories#################

ls -d */

#####list files sorted by size####
 ls -lS -h

#####generar árboles filogeneticos con clustalw y visualizarlos en Figtree####
clustalw -INFILE=step2_rep_set.fna -TREE -TYPE=DNA -OUTPUTTREE=nj  ###genera dos archivos con el mismo nombre pero con extensión .nj y .ph

java -jar /home/marcos/Documents/FigTree_v1.4.2/lib/figtree.jar step2_rep_set.ph 

###extract the last fields in taxonomy file###

more NCBI_MG_uniques.tax | rev | cut -d ';' -f1-3 | rev > gen_spec.tax

###combine text files in only one tab separated columns file####

paste ID_NCBI_tax gen_spec.tax | column -s $'\t' -t > NCBI_MG_uniq_gen_spec.tax

###transpose from space separated columns to lines###

tr ' ' '\n' file

####extract duplicated headers in fasta####

 grep ">" NCBI_MG.fa | sed 's/>//g' | sort | uniq -d > dup_fasta

#####remove duplicates headers in fasta file#####




####USO DE QIIME####

#closed reference###

pick_closed_reference_otus.py -i ../r01072016pe.trim.contigs.good.fasta -f -o ./ucrC97/ -p otu_picking_params_97.txt -r w2_uniques.fasta -t w2_uniques.tax

#open reference####

pick_open_reference_otus.py -i r01072016pe.trim.contigs.good.fasta -o ./uc_open_ref97/ -p otu_picking_params_97.txt -r w2_uniques.fasta -t w2_uniques.tax

####converting biom files####

biom convert -i table.biom -o table.from_biom.txt --to-tsv

####revision SAM files####

samtools view -S T24POST_S4_L001.sam | cut -f12 -d$'\t' | grep -c "XS"



##Localizar y mover archivos:
#forward
find . -type f -name "*R1_001.fastq.gz" | xargs cp -t /home/marcos/Documents/XIXIMI/run_07_11_2016/forward_reads/
#reverse
find . -type f -name "*R2_001.fastq.gz" | xargs cp -t /home/marcos/Documents/XIXIMI/run_07_11_2016/reverse_reads/

find . -iname '*.fastq.gz' -exec mv '{}' . \;

#Descomprimir archivos

find . -name "*.gz" -exec gunzip {} \; 


###Blast en OMICA###

/LUSTRE/bioinformatica_data/genomica_funcional/bin/ncbi-blast-2.4.0+/bin/blastx -query /LUSTRE/bioinformatica_data/genomica_funcional/Laura/Transcriptoma_completo/Trinity_Ensamble_3Transcriptomas/Transcriptoma_completo.fasta -db /LUSTRE/bioinformatica_data/BD/NR/nr.54 -num_threads 24 -max_target_seqs 1 -evalue 1E-05 -outfmt 5 > NRTranscomplete_blastx.xml


###COntar columnas en bash####

sed 's/\t/\n/g' *unique_list*.shared | wc -l

##contar solo caracteres alfanumericos con expresión regular####

cut -f12 All_matrix.tsv | grep "[/^\w]" | sort -u | wc -l

####sleccionar y ordenar un archivo .tsv basado en una columna (12)#####
grep "[/^\w]" All_matrix.tsv | sort -u -k12,12 | wc -l

##seleccionar unicas basadas en 2 columnas##

sort -u -k5,5 -k6,6 All_spec_taxon.csv > spec_uniq_taxon


####"X" remotion from deVargas database####

sed 's/;*_X;/;Unknown;/g; s/;*_X_sp.;/;Unknown;/g; s/;*_XX;/;Unknown;/g; s/;*_XX_sp.;/;Unknown;/g; s/;*_XXX;/;Unknown;/g; s/;*_XXX_sp.;/;Unknown;/g; s/;*_XXXX;/;Unknown;/g; s/;*_XXXX_sp.;/;Unknown;/g; s/;*_XXXXX;/;Unknown;/g; s/;*_XXXXX_sp.;/;Unknown;/g; s/;*_XXXXXX;/;Unknown;/g; s/;*_XXXXXX_sp.;/;Unknown;/g' w2_worms_uniques.tax > w2_worms_uniques_noX.tax

sed -i 's/Stramenopiles_X-Group-3/Unknown/g' w2_worms_uniques_noX.tax 

sed -i 's/Arachnida_X_sp/Unknown/g' w2_worms_uniques_noX.tax 

sed -i 's/Mastigamoebida_XX_simplex/Unknown/g' w2_worms_uniques_noX.tax

sed -i 's/Acanthamoebidae_X_arachisporum/Unknown/g' w2_worms_uniques_noX.tax 

sed -i 's/Annelida_XXX_sp/Unknown/g' w2_worms_uniques_noX.tax 

sed -i 's/Tardigrada_XxX/Unknown/g' w2_worms_uniques_noX.tax

sed -i 's/Tardigrada_XxX_sp./Unknown/g' w2_worms_uniques_noX.tax 

sed -i 's/Choanoflagellatea_X_Group_P/Unknown/g' w2_worms_uniques_noX.tax 

 sed -i 's/Stramenopiles_X-Group-4/Unknown/g' w2_worms_uniques_noX.tax 

sed -i 's/Embryophyceae_XXX_sp/Unknown/g' w2_worms_uniques_noX.tax 

